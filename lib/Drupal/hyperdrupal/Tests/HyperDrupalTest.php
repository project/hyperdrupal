<?php

/**
 * @file
 * Contains Drupal\hyperdrupal\Tests\HyperDrupalTest.
 */

namespace Drupal\hyperdrupal\Tests;

use Drupal\simpletest\WebTestBase;

/**
 * Tests HyperDrupal installation profile expectations.
 */
class HyperDrupalTest extends WebTestBase {

  protected $profile = 'hyperdrupal';

  public static function getInfo() {
    return array(
      'name' => 'HyperDrupal installation profile',
      'description' => 'Tests HyperDrupal installation profile expectations.',
      'group' => 'HyperDrupal',
    );
  }

  /**
   * Tests HyperDrupal installation profile.
   */
  function testHyperDrupal() {
    // Test anonymous user can access 'Main navigation' block.
    $admin = $this->drupalCreateUser(array('administer blocks'));
    $this->drupalLogin($admin);
    // Configure the block.
    $this->drupalGet('admin/structure/block/add/system_menu_block:menu-main/bartik');
    $this->drupalPost(NULL, array(
      'region' => 'sidebar_first',
      'machine_name' => 'main_navigation',
    ), t('Save block'));
    // Verify admin user can see the block.
    $this->drupalGet('');
    $this->assertText('Main navigation');

    // Verify we have role = aria on system_powered_by and system_help_block
    // blocks.
    $this->drupalGet('admin/structure/block');
    $elements = $this->xpath('//div[@role=:role and @id=:id]', array(
      ':role' => 'complementary',
      ':id' => 'block-help',
    ));

    $this->assertEqual(count($elements), 1, 'Found complementary role on help block.');

    $this->drupalGet('');
    $elements = $this->xpath('//div[@role=:role and @id=:id]', array(
      ':role' => 'complementary',
      ':id' => 'block-powered',
    ));
    $this->assertEqual(count($elements), 1, 'Found complementary role on powered by block.');

    // Verify anonymous user can see the block.
    $this->drupalLogout();
    $this->assertText('Main navigation');

  }

}
