<?php
/**
 * @file
 * Enables modules and site configuration for a HyperDrupal installation.
 */

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 *
 * Allows the profile to alter the site configuration form.
 */
function hyperdrupal_form_install_configure_form_alter(&$form, $form_state) {
  // Pre-populate the site name with the server name.
  $form['site_information']['site_name']['#default_value'] = 'HyperDrupal';

  // Use an east coast American example configuration on installation.
  $form['regional_settings']['site_default_country']['#default_value'] = 'US';
  $form['regional_settings']['date_default_timezone']['#default_value'] = 'America/New_York';

  // Disable update notifications at least until we have tagged releases of all
  // modules to test from.
  $form['update_notifications']['update_status_module']['#default_value'] = array();
}
